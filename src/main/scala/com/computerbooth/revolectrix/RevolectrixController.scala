package com.computerbooth.revolectrix

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.SpringApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.annotation.PreDestroy
import org.springframework.context.annotation.Configuration
import jssc.SerialPortList
import com.computerbooth.revolectrix.actors.RevolectrixActor
import com.computerbooth.revolectrix.actors.RevolectrixActorProps

@SpringBootApplication
class RevolectrixController extends CommandLineRunner {
  val logger = LoggerFactory getLogger classOf[RevolectrixController]
  
  @Bean
  def system(): ActorSystem = ActorSystem("ControllerSystem")
  
  def revolectrixActorRef(portName:String): ActorRef = {
    system.actorOf(revoletrixActorProps.props(portName))
  }
  
  @Autowired
  var applicationContext: ApplicationContext = null
  
  @Autowired
  var revoletrixActorProps: RevolectrixActorProps = null

  override def run(args: String*): Unit = {
    val ports = SerialPortList.getPortNames
    logger.debug("Found ports {}", ports)
    ports filter { portName => portName contains "/dev/ttyUSB" } foreach { portName => {
      logger.debug("Loading with port {}", portName)
      revolectrixActorRef(portName)
    }}
  }
}

object RevolectrixController {
  def main(args: Array[String]): Unit = {
    SpringApplication run classOf[RevolectrixController]
  }
}