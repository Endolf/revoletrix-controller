package com.computerbooth.revolectrix.actors

import akka.actor.ActorLogging
import akka.actor.Actor
import jssc.SerialPortList
import org.slf4j.LoggerFactory
import jssc.SerialPort
import scala.concurrent.duration._
import akka.actor.Cancellable
import jssc.SerialPortEventListener
import jssc.SerialPortEvent
import akka.actor.ActorRef
import scala.collection.GenSeq
import akka.actor.Terminated
import akka.actor.PoisonPill
import java.nio.charset.Charset
import org.springframework.context.annotation.Bean
import akka.actor.Props
import org.springframework.stereotype.Service

case class BufferReady(data:Array[Byte])
case class CountBytes()
case class GetOptions()

@Service
class RevolectrixActorProps {
  def props(portName:String): Props = Props(new RevolectrixActor(portName))
}

class RevolectrixActor(portName:String) extends Actor with ActorLogging {
  import context.dispatcher

  private val logger = LoggerFactory getLogger classOf[RevolectrixActor].getCanonicalName + " (" + portName + ")"
//  private val ticker = Option(context.system.scheduler.schedule(1000 millis, 1000 millis, self, CountBytes))

  private var serialPort: Option[SerialPort] = null  
  private var lastSeenChargerName = "";

  override def preStart(): Unit = {
    serialPort = Option(new SerialPort(portName))
    serialPort.foreach { sp =>
      sp.openPort(); //Open serial port
      sp.setParams(SerialPort.BAUDRATE_19200,
        SerialPort.DATABITS_8,
        0,
        SerialPort.PARITY_NONE)
      sp.purgePort(SerialPort.PURGE_RXCLEAR + SerialPort.PURGE_TXCLEAR)
      sp.addEventListener(new ByteCountingSerialPortEventListener(160,context self, sp))
    }
    context become syncing(5)
  }

  override def postStop(): Unit = {
    logger.debug("Stopped")
    serialPort.filter( sp => sp.isOpened()).foreach { sp => sp.closePort() }
  }

  def syncing(failedSyncRemaining: Int): Receive = {
    case BufferReady(bytes) => {
      serialPort.filter { sp => sp.isOpened() } foreach { sp => {
        if(failedSyncRemaining<0) {
          logger.error("Failed to sync - too many attempts, giving up")
          self ! PoisonPill
        } else {
          logger.debug("Syncing ({})", failedSyncRemaining)
          var data = bytes
          if (data.length == 160 && data.startsWith(GenSeq(0x72, 0x43, 0x53, 0x1))) {
            logger.debug("Synchronised")
            context become idle
            self ! GetOptions
          } else if (data.contains(0x72)) {
            val offset = data.indexOf(0x72)
            if (offset > 0) {
              val x = offset + 10;
              logger.debug("Skipping {} bytes", x)
              data = (data ++ sp.readBytes(x) splitAt(x))._2
            }
            val index = data(3)
            if(index > 16) {
              logger.debug("Index was {}, trying to sync again")
            } else if (index != 1) {
              val y = (17 - index) * 10;
              logger.debug("Skipping {} bytes, index was {}", y, index)
              sp.readBytes(y)
            }
            context become syncing(failedSyncRemaining - 1)
          } else {
            if (failedSyncRemaining == 1) {
              logger.error("Failed to sync - couldn't find start bytes in data {}", data)
              self ! PoisonPill
            } else {
              logger.debug("Couldn't find start bytes in data {}", data)
              context become syncing(failedSyncRemaining - 1)
            }
          }
        }}
      }
    }
    case CountBytes => logger.debug("{} bytes in buffer", serialPort.filter { port => port.isOpened() } map { port => port.getInputBufferBytesCount()})
    case other => logger.debug("Got unexpected message {} whilst syncing", other)
  }

  def idle: Receive = {
    case BufferReady(bytes) => {
      serialPort.filter { sp => sp.isOpened() } foreach { sp => {
        var data = bytes
        if (data.length == 160 && data.startsWith(GenSeq(0x72, 0x43, 0x53, 0x1))) {
          logger.debug("Got heartbeat whilst idle")
        } else {
          logger.debug("Lost sync whilst idle")
          sendGetHeartbeat(sp)
          context become syncing(5)
        }
      }}
    }
    case GetOptions => context become getOptions
    case CountBytes => logger.debug("{} bytes in buffer", serialPort.filter { port => port.isOpened() } map { port => port.getInputBufferBytesCount()})
    case other => logger.debug("Got unexpected message {} whilst idle", other)
  }
  
  def getOptions: Receive = {
    case BufferReady(bytes) if bytes.length==160 => syncAndSend(bytes, sendGetOptions)
    case BufferReady(bytes) if bytes.length==262 && !bytes.containsSlice(GenSeq(0x72, 0x43, 0x53, 0x1)) => {
      serialPort.filter { sp => sp.isOpened() } foreach { sp => {
        val data = bytes map { x => x.toInt } map { x => if(x<0) 256 + x else x} 
        logDataBuffer(data, "Got options data")
        val nameBytes = bytes drop 136 take 32
        val nameBytesOrdered: Array[Byte] = new Array(nameBytes.length)
        for(x <- 0 until (nameBytes.length/2)) {
          nameBytesOrdered.update(x*2, nameBytes((x*2)+1))
          nameBytesOrdered.update((x*2)+1, nameBytes(x*2))
        }
        lastSeenChargerName = new String(nameBytesOrdered, Charset.forName("UTF-8")).trim
        logger.debug("Charger name \"{}\"", lastSeenChargerName)
        sendGetHeartbeat(sp)
        context become getStatus
      }}      
    }
    case BufferReady(bytes) => logBufferAndResync(bytes, "Got data whilst waiting for options")
    case other => logger.debug("Got unexpected message {} whilst idle", other)
  }
  
  def getStatus: Receive = {
    case BufferReady(bytes) if bytes.length==160 => syncAndSend(bytes, sendGetStatus(0))
    case BufferReady(bytes) if bytes.length==153 && !bytes.containsSlice(GenSeq(0x72, 0x43, 0x53, 0x1)) => {
      serialPort.filter { sp => sp.isOpened() } foreach { sp => {
        val data = bytes map { x => x.toInt } map { x => if(x<0) 256 + x else x} 
        
        logDataBuffer(data, "Got status data")

        val pl8CRCInitialValue = 2342
        val pl6CRCInitialValue = 4742
        
        val crcInitialValue = lastSeenChargerName match {
          case "PowerLab 6   Firmware   V1.00" => {pl6CRCInitialValue}
          case "PowerLab 8v2  Firmware" => {pl8CRCInitialValue}
          case _ => 0
        }
        
        logger.debug("Initialising CRC to {}", crcInitialValue)
        
        val calcCRC = data.slice(4, 151).foldLeft(crcInitialValue)(RXCRC16)
        val messageCRC = (data(151) * 256) + data(152)
        logger.debug("CRC was {}, calculated {}", messageCRC, calcCRC)
        
        val chargerNum = data(3)
        val firmwareVersion = ((data(4) * 256) + data(5))/100f
        val temp = (2.5 * ((data(30) * 256) + data(31)) / 4095 - 0.986) / 0.00355
        
        logger.debug("Charger {}, firmware version {}", chargerNum, firmwareVersion) 
        logger.debug("Charger {}, CPU temp {}", chargerNum, temp) 
        sendGetHeartbeat(sp)
      }}
      context become idle
    }
    case BufferReady(bytes) => logBufferAndResync(bytes, "Got data whilst waiting for status")
    case other => logger.debug("Got unexpected message {}", other)
  }
  
  def syncAndSend(bytes:Array[Byte], sendFunction: SerialPort=>Unit): Unit = {
      serialPort.filter { sp => sp.isOpened() } foreach { sp => {
        var data = bytes
        if (data.length == 160 && data.startsWith(GenSeq(0x72, 0x43, 0x53, 0x1))) {
          sendFunction(sp)
        } else {
          logger.debug("Lost sync waiting for options")
          sendGetHeartbeat(sp)
          context become syncing(5)
        }
      }}
  }
  
  def logDataBuffer(data:Array[Int], baseMessage:String):Unit = {
    logger.debug("{} {} bytes: {}", baseMessage, "" + data.length, data map {x => {
      if(x<16) "0" + Integer.toHexString(x) toUpperCase
      else Integer.toHexString(x) toUpperCase
    }} mkString " ")
  }
  
  def logBufferAndResync(data:Array[Byte], baseMessage:String):Unit = {
    val bytes = data map { x => x.toInt } map { x => if(x<0) 256 + x else x}
    logDataBuffer(bytes, baseMessage)
    context become syncing(5)
  }

  def receive = {
    case other => logger.debug("Got unexpected message {}", other)
  }
  
  def sendGetHeartbeat(sp:SerialPort):Unit = {
    sp.removeEventListener()
    sp.addEventListener(new ByteCountingSerialPortEventListener(160,self,sp))
  }
  
  def sendGetOptions(sp:SerialPort) = {
    sp.removeEventListener()
    sp.addEventListener(new ByteCountingSerialPortEventListener(262,self,sp))
    logger.debug("Get options")
    sp.writeString("PrsI")
  }

  def sendGetStatus(charger:Byte)(sp:SerialPort) = {
    sp.removeEventListener()
    sp.addEventListener(new ByteCountingSerialPortEventListener(153,self,sp))
    logger.debug("Get status \"{}\"", charger)
    sp.writeString("Ram")
    sp.writeByte(charger)
  }
  
  def RXCRC16(crc:Int, input:Int):Int = {
    var workingInput = input
    var workingCRC = crc
    for (x <- 1 to 8) {
      val temp = workingInput ^ workingCRC
      if((temp/2)==((temp-1)/2)) {
        workingCRC = (workingCRC/2)
        workingCRC = 33800 ^ workingCRC
      } else {
        workingCRC = (workingCRC/2)
      }
      workingInput = (workingInput/2)
    }
    workingCRC
  }
}

private class ByteCountingSerialPortEventListener(bytesToCount:Int, actorRef:ActorRef, serialPort: SerialPort) extends SerialPortEventListener {
  private val logger = LoggerFactory getLogger classOf[ByteCountingSerialPortEventListener]

  override def serialEvent(serialPortEvent:SerialPortEvent): Unit = {
    if(serialPortEvent.getEventType==SerialPortEvent.RXCHAR && serialPortEvent.getEventValue>=bytesToCount) {
      actorRef ! BufferReady(serialPort.readBytes(bytesToCount))
    }
  }
}
